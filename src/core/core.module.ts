import { Module } from '@nestjs/common';
import { SocketGateway } from './gateways/socket.gateway';
import { LogicModule } from '../logic/logic.module';

@Module({
  imports: [
    LogicModule,
  ],
  providers: [
    SocketGateway,
  ],
})
export class CoreModule {}
