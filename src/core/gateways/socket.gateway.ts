import { OnGatewayDisconnect, SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { PlayerService } from '../../logic/services/player.service';
import { Player } from '../../logic/entities/player.entity';
import { TickUpdateService } from '../../logic/services/tickupdate.service';
import { GameService } from '../../logic/services/game.service';
import { Equipment, PickUp } from '../../logic/models/game.model';

@WebSocketGateway(8084, {
  pingTimeout: 5000,
  pingInterval: 10000,
})
export class SocketGateway implements OnGatewayDisconnect {

  constructor(
    private readonly playerService: PlayerService,
    private readonly tickUpdateService: TickUpdateService,
    private readonly gameService: GameService,
  ) {
  }

  handleDisconnect(client: Socket): void {
    this.playerService.playerQuit(client);
  }

  @SubscribeMessage('join')
  onJoin(client: Socket, data: Player): void {
    this.playerService.playerJoined(client, data);
  }

  @SubscribeMessage('quit')
  onQuit(client: Socket): void {
    this.playerService.playerQuit(client);
  }

  @SubscribeMessage('movement')
  onMove(client: Socket, data: any): void {
    this.tickUpdateService.accumulator[data.playerId] = data;
  }

  @SubscribeMessage('toggle-ready')
  onToggleReady(client: Socket): void {
    this.playerService.toggleReady(client);
  }

  @SubscribeMessage('equipment-changed')
  onEquipmentChange(client: Socket, data: Equipment): void {
    console.log(data);
    this.gameService.changeEquipment(client, data);
  }

  @SubscribeMessage('pick-up')
  onPickUp(client: Socket, data: PickUp): void {
    this.gameService.pickUp(client, data);
  }
}
