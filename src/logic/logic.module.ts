import { Module } from '@nestjs/common';
import { PlayerService } from './services/player.service';
import { ConnectionService } from './services/connection.service';
import { DataService } from './services/data.service';
import { TickUpdateService } from './services/tickupdate.service';
import { GameService } from './services/game.service';

@Module({
  imports: [],
  providers: [
    PlayerService,
    ConnectionService,
    DataService,
    TickUpdateService,
    GameService,
  ],
  exports: [
    PlayerService,
    ConnectionService,
    DataService,
    TickUpdateService,
    GameService,
  ],
})
export class LogicModule {}
