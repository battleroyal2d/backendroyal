import { Injectable } from '@nestjs/common';
import { ConnectionService } from './connection.service';
import { Status } from '../models/game.model';

@Injectable()
export class TickUpdateService {

  public accumulator: { [key: string]: Status; } = {};

  constructor(
    private readonly connectionService: ConnectionService,
  ) {
    this.accumulator = {};
    setInterval(() => {
      const keys = Object.keys(this.accumulator);
      const data: Status[] = [];
      keys.forEach((key: string) => {
        data.push(this.accumulator[key]);
      });
      keys.forEach((key: string) => {
        const player = this.connectionService.getPlayer(key);
        if (player) {
          const status = this.accumulator[key];
          player.posX = status.x;
          player.posY = status.y;
        }

        this.connectionService.broadcastInRange('status', data, 1000, key);
        delete this.accumulator[key];
      });
    }, 30);
  }
}
