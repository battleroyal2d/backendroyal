import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { Player } from '../entities/player.entity';

@Injectable()
export class ConnectionService {

  private onlinePlayers = new Map<string, Player>();
  private clients: Socket[] = [];

  addPlayer(player: Player): void {
    this.onlinePlayers.set(player.id, player);
    this.clients.push(player.client);
  }

  removePlayer(id: string): Player {
    const removedPlayer = this.onlinePlayers.get(id);

    if (!removedPlayer) {
      return null;
    }

    this.onlinePlayers.delete(id);
    this.clients = this.clients.filter((client: Socket) => {
      return client.id !== id;
    });

    return removedPlayer;
  }

  getPlayer(playerId: string): Player | null {
    return this.onlinePlayers.get(playerId);
  }

  getOnlinePlayers(): Player[] {
    const players = [];
    this.onlinePlayers.forEach((player: Player) => {
      players.push({
        name: player.name,
        id: player.id,
        posX: player.posX,
        posY: player.posY,
      } as Player);
    });

    return players;
  }

  getOnlinePlayersExcept(id: string): Player[] {
    return this.getOnlinePlayers()
      .filter((onlinePlayer: Player) => {
        return onlinePlayer.id !== id;
      });
  }

  broadcast(event: string, data?: any): void {
    this.clients.forEach((client: Socket) => {
      client.emit(event, data);
    });
  }

  broadcastWithout(event: string, data: any, playerId: string): void {
    this.clients.forEach((client: Socket) => {
      if (client.id !== playerId) {
        client.emit(event, data);
      }
    });
  }

  broadcastInRange(event: string, data: any, range: number, playerId: string): void {
    const centerPlayer = this.getPlayer(playerId);

    this.getOnlinePlayersExcept(playerId).forEach((player: Player) => {
      if (this.isInRange(centerPlayer, player, range)) {
        this.send(event, data, player.id);
      }
    });
  }

  send(event: string, data: any, playerId: string): void {
    const player = this.getPlayer(playerId);
    if (player) {
      player.client.emit(event, data);
    }
  }

  private isInRange(centerPlayer: Player, targetPlayer: Player, radius: number): boolean {
    let distance;
    try {
      distance = Math.sqrt((centerPlayer.posX - targetPlayer.posX) ** 2 + (centerPlayer.posY - targetPlayer.posY) ** 2);
    } catch (e) {
      return false;
    }
    return distance <= radius;
  }
}
