import { Injectable } from '@nestjs/common';
import { ConnectionService } from './connection.service';
import { Socket } from 'socket.io';
import { Equipment, PickUp, Status } from '../models/game.model';

@Injectable()
export class GameService {

  constructor(
    private readonly connectionService: ConnectionService,
  ) {}

  changeEquipment(client: Socket, data: Equipment): void {
    const player = this.connectionService.getPlayer(client.id);
    player.equipment = data;
    const status = {
      playerId: player.id,
      equipmentChanged: data,
    } as Status;

    this.connectionService.broadcastWithout('status', status, player.id);
  }

  pickUp(client: Socket, data: PickUp): void {
    console.log(data);
    this.connectionService.broadcastWithout('item-pick-up', data, client.id);
  }
}
