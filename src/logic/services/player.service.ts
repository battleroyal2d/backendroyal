import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { ConnectionService } from './connection.service';
import { Player } from '../entities/player.entity';
import { DataService } from './data.service';

@Injectable()
export class PlayerService {

  private readyPlayers: string[] = [];

  constructor(
    private readonly connectionService: ConnectionService,
    private readonly dataService: DataService,
  ) {}

  playerJoined(client: Socket, player: Player): void {
    if (this.dataService.gameState === 'ingame') {
      client.disconnect(true);
      return;
    }

    player.client = client;
    player.id = client.id;
    player.health = 100;
    this.connectionService.addPlayer(player);
    this.connectionService.send('connected', {
      id: player.id,
      name: player.name,
    }, player.id);

    console.log(`${player.name} (${player.id}) connected`);

    this.connectionService.broadcastWithout('player-joined', {
      name: player.name,
      id: player.id,
    }, player.id);

    const onlinePlayers = this.connectionService.getOnlinePlayersExcept(player.id);

    this.connectionService.send('online-players', onlinePlayers, player.id);
  }

  playerQuit(client: Socket): void {
    const player = this.connectionService.removePlayer(client.id);
    if (player === null) {
      return;
    }

    console.log(`${player.name} (${player.id}) disconnected`);

    if (this.connectionService.getOnlinePlayers().length === 0 && this.dataService.gameState === 'ingame') {
      console.log('All player left, restarting game');
      this.dataService.gameState = 'lobby';
      this.readyPlayers = [];
      // process.exit(1);
      return;
    }

    this.connectionService.broadcast('player-quit', {
      id: player.id,
    });
  }

  toggleReady(client: Socket): void {
    const index = this.readyPlayers.indexOf(client.id);
    if (index !== -1) {
      this.readyPlayers.splice(index, 1);
    } else {
      this.readyPlayers.push(client.id);
    }

    if (this.connectionService.getOnlinePlayers().length === this.readyPlayers.length) {
      if (this.dataService.gameState === 'ingame' || this.connectionService.getOnlinePlayers().length < 2) {
        return;
      }
      console.log('Game is starting');
      this.connectionService.broadcast('start-game');
      this.dataService.gameState = 'ingame';
    }
  }
}
