import { Injectable } from '@nestjs/common';

@Injectable()
export class DataService {

  public gameState: 'lobby' | 'ingame' = 'lobby';

}
