import { Socket } from 'socket.io';
import { Equipment } from '../models/game.model';

export class Player {

  id: string;
  name: string;
  client: Socket;

  posX: number;
  posY: number;

  health: number;
  equipment: Equipment;
}
