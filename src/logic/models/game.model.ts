export interface Status {

  playerId: string;

  x: number;
  y: number;

  c: number;
  w: number;

  equipmentChanged?: Equipment;
}

export interface Equipment {

  head: string;
  chest: string;
  weapon: string;
  shield: string;
  feet: string;
}

export interface PickUp {
  itemId: string;
}
